const app = require("./server"); // Link to your server file
const supertest = require("supertest");
const request = supertest(app);

test("Call the / endpoint", async () => {
  const res = await request.get("/");
  expect(res.status).toBe(200);
  expect(res.text).toBe("This App is running properly!");
});
test("Call the /pong endpoint", async () => {
  const res = await request.get("/ping");
  expect(res.status).toBe(200);
  expect(res.text).toBe("Pong!");
});
test("Call the /hello/:name endpoint", async () => {
  const res = await request.get("/hello/Iqbal");
  expect(res.status).toBe(200);
  expect(res.body.message).toBe("Hello Iqbal");
});
